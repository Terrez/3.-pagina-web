**Trabajo realizado por:**
==========================

C.A. Miguel Ignacio Terrez Lopez

Fecha: 14/02/2014

**Trabajo realizado sobre:**
============================

Academia Log�stica de Calatayud

**Procedimiento para iniciar la p�gina web:**
=============================================

Ejecute "Index.html" para iniciar la "Web". Abrirlo con el navegador "Google Chrome"

**Conclusiones:**
=================

El trabajo ha sido bastante duro de iniciar y de finalizar por la amplitud de posibilidades, conocimientos y requerimientos del mismo. Ha sido intelectualmente muy enriquecedor, proporcion�ndome diversos conocimientos sobre "html 4 y 5" y "CSS". Esta tarea me va a servir aparte de lo anteriormente dicho para concluir la tarea 5 en la cual debemos de crear una web m�s extensa y elaborada.